#!../../bin/linux-x86_64/st

## You may have to change st to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/st.dbd"
st_registerRecordDeviceDriver pdbbase

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "2000000")
epicsEnvSet("SIZE", "1800000")

## test
epicsEnvSet("IOCBL", "LPQ")
epicsEnvSet("IOCDEV", "target")

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
dbLoadRecords("target.db","BL=$(IOCBL),DEV=$(IOCDEV)")
## target 1
dbLoadRecords("target_unit.db","BL=$(IOCBL),DEV=$(IOCDEV),N=1,SIZE=$(SIZE)")
dbLoadRecords("zone.db","BL=$(IOCBL),DEV=$(IOCDEV),N=1,Z=1")
dbLoadRecords("zone.db","BL=$(IOCBL),DEV=$(IOCDEV),N=1,Z=2")
dbLoadRecords("shot.db","BL=$(IOCBL),DEV=$(IOCDEV),N=1")
## target 2
dbLoadRecords("target_unit.db","BL=$(IOCBL),DEV=$(IOCDEV),N=2,SIZE=$(SIZE)")
dbLoadRecords("zone.db","BL=$(IOCBL),DEV=$(IOCDEV),N=2,Z=1")
dbLoadRecords("zone.db","BL=$(IOCBL),DEV=$(IOCDEV),N=2,Z=2")
dbLoadRecords("shot.db","BL=$(IOCBL),DEV=$(IOCDEV),N=2")

## autosave
set_savefile_path("/EPICS/autosave")
set_requestfile_path("${TOP}/iocBoot/${IOC}")
#set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

## autosave
create_monitor_set("auto_settings.req", 30, "BL=$(IOCBL), DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"

