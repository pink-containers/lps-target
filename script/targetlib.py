import time
import epics
import numpy as np

def compressmap(data):
  target = data['target']
  mdim = data['mdim']

  ## only process if map 1 or 2 was changed
  if target==1:
    mid = 0
  elif target==2:
    mid = 1
  else:
    return

  NY = int(mdim[0])
  NX = int(mdim[1])
  DNY = int(np.floor(NY/2))
  DNX = int(np.floor(NX/2))

  srcflat = data['outmap'][mid].get()
  src = srcflat.reshape((NY,NX))

  dest = np.zeros((DNY,DNX), dtype=np.uint8)

  for i in range(DNY):
    for j in range(DNX):
      v1=max(src[(2*i),(2*j)],src[(2*i),(2*j)+1])
      v2=max(src[(i*2)+1,j*2],src[(i*2)+1,(2*j)+1])
      v3=max(v1,v2)
      dest[i,j]=v3

  data['outmapcomp'][mid].put(dest.flatten())

def pos2index(pos,data):
  xscale = 0.1
  yscale = 0.2
  ymax,xmax=data['mdim']
  t0,t1,r0,r1=pos
  x0=min(int(round(t0/xscale)),xmax-1)
  x1=min(int(round(t1/xscale)),xmax-1)
  y0=min(int(round(r0/yscale)),ymax-1)
  y1=min(int(round(r1/yscale)),ymax-1)
  return (x0,x1,y0,y1)

def idscheck(ids):
  x0,x1,y0,y1=ids
  xchk=False
  ychk=False
  if(x1-x0)>=0:
    xchk=True
  if(y1-y0)>0:
    ychk=True
  return (xchk and ychk)

def draw(setlist, data):
  mdim = data['mdim']
  res=False
  for s in setlist:
   pos, color, map, outmappv = s
   ids = pos2index(pos,data)
   if idscheck(ids):
     x0,x1,y0,y1=ids
     dx=x1-x0+1
     dy=y1-y0
     dim=(dy,dx)
     overlay=color*np.ones(dim,dtype=np.uint8)
     mat = map.reshape(data['mdim'])
     mat[y0:(y0+dy),x0:(x0+dx)]=overlay
     data['maplist'][data['mid']]=mat.flatten()
     outmappv.put(data['maplist'][data['mid']])
     res=True
  return res

def tracker(data):
  isshot=False
  ## move to last
  if data['target']==-1:
    #print("move to last")
    target = data['targetpv'].get()
    tgst=int(target+1)
    tt = epics.caget(f"LPQ:target:{tgst}:ttLast")
    tr = epics.caget(f"LPQ:target:{tgst}:trLast")
    epics.caput("LPQ:source:axis:2:pos_abs", tt)
    epics.caput("LPQ:source:axis:1:pos_abs", tr)
    #print(f"tt:{tt}, tr:{tr}")
  ## clear
  elif (data['set']==False):
    target=data['target']
    zone=data['zone']
    # choose correct map
    if target==1 and zone==False:
      mid=0
    elif target==2 and zone==False:
      mid=1
    elif target==1 and zone==True:
      mid=2
    elif target==2 and zone==True:
      mid=3
    else:
      print("No map could be matched. Returning!")
      return
    data['maplist'][mid]*=0
    data['mappvlist'][mid].put(data['maplist'][mid])
  ## set
  elif (data['set']==True):
    target=data['target']
    zone=data['zone']
    # get active target if general set button
    if target==0:
      ## shot redirector
      # check enable
      if data['enablepv'].get()==0:
        return
      target=int(data['targetpv'].get()+1)
      zone=False
      needarea=False
      isshot=True
      t0 = epics.caget("LPQ:target:ttBegin")
      t1 = epics.caget("LPQ:target:ttEnd")
      r0 = epics.caget("LPQ:target:trBegin")
      r1 = epics.caget("LPQ:target:trEnd")
      area = [t0,t1,r0,r1]
    else:
      needarea=True
    # choose correct map
    if target==1 and zone==False:
      #shot1
      mid=0
    elif target==2 and zone==False:
      #shot2
      mid=1
    elif target==1 and zone==True:
      #base1
      mid=2
    elif target==2 and zone==True:
      #base2
      mid=3
    else:
      print("No map could be matched. Returning!")
      return
    map=data['maplist'][mid]
    outmap=data['mappvlist'][mid]
    setlist = []
    # get correct set coordenates
    if mid==0:
      if needarea:
        t0 = epics.caget("LPQ:target:1:shot:ttBegin")
        t1 = epics.caget("LPQ:target:1:shot:ttEnd")
        r0 = epics.caget("LPQ:target:1:shot:trBegin")
        r1 = epics.caget("LPQ:target:1:shot:trEnd")
        area = [t0,t1,r0,r1]
      color = 10
      setlist.append([area,color,map,outmap])
    elif mid==1:
      if needarea:
        t0 = epics.caget("LPQ:target:2:shot:ttBegin")
        t1 = epics.caget("LPQ:target:2:shot:ttEnd")
        r0 = epics.caget("LPQ:target:2:shot:trBegin")
        r1 = epics.caget("LPQ:target:2:shot:trEnd")
        area = [t0,t1,r0,r1]
      color = 10
      setlist.append([area,color,map,outmap])
    elif mid==2:
      t0 = epics.caget("LPQ:target:1:Z1:ttBegin")
      t1 = epics.caget("LPQ:target:1:Z1:ttEnd")
      r0 = epics.caget("LPQ:target:1:Z1:trBegin")
      r1 = epics.caget("LPQ:target:1:Z1:trEnd")
      area = [t0,t1,r0,r1]
      color = epics.caget("LPQ:target:1:Z1:material")+2
      setlist.append([area,color,map,outmap])
      z2 = epics.caget("LPQ:target:1:Z2:enable")
      if z2>0:
        t0 = epics.caget("LPQ:target:1:Z2:ttBegin")
        t1 = epics.caget("LPQ:target:1:Z2:ttEnd")
        r0 = epics.caget("LPQ:target:1:Z2:trBegin")
        r1 = epics.caget("LPQ:target:1:Z2:trEnd")
        area = [t0,t1,r0,r1]
        color = epics.caget("LPQ:target:1:Z2:material")+2
        setlist.append([area,color,map,outmap])
    elif mid==3:
      t0 = epics.caget("LPQ:target:2:Z1:ttBegin")
      t1 = epics.caget("LPQ:target:2:Z1:ttEnd")
      r0 = epics.caget("LPQ:target:2:Z1:trBegin")
      r1 = epics.caget("LPQ:target:2:Z1:trEnd")
      area = [t0,t1,r0,r1]
      color = epics.caget("LPQ:target:2:Z1:material")+2
      setlist.append([area,color,map,outmap])
      z2 = epics.caget("LPQ:target:2:Z2:enable")
      if z2>0:
        t0 = epics.caget("LPQ:target:2:Z2:ttBegin")
        t1 = epics.caget("LPQ:target:2:Z2:ttEnd")
        r0 = epics.caget("LPQ:target:2:Z2:trBegin")
        r1 = epics.caget("LPQ:target:2:Z2:trEnd")
        area = [t0,t1,r0,r1]
        color = epics.caget("LPQ:target:2:Z2:material")+2
        setlist.append([area,color,map,outmap])
    #print(f"set : target={target} : zone={zone} : mid={mid}")
    #print(setlist)
    data['mid']=mid
    res=draw(setlist, data)
    if (res and isshot):
      ltt = epics.caget("LPQ:target:ttEnd")
      ltr = epics.caget("LPQ:target:trEnd")
      tgst=int(data['targetpv'].get()+1)
      epics.caput(f"LPQ:target:{tgst}:ttLast",ltt)
      epics.caput(f"LPQ:target:{tgst}:trLast",ltr)
  else:
    print("[{}] Error: Could not match condition".format(time.asctime()))

##EOF
