#!/usr/bin/python3
import epics
import numpy as np
import threading
import time
from targetlib import *

print("*** Target shot tracker service ***")
print("Date: {}".format(time.asctime()))

## globals
ASIZE=1800000
NX = 1000
NY = 1800
mdim = (NY,NX)

# data
data = {}
# event
eve = threading.Event()

# callbacks
def cb_shotupdate(**kws):
  if kws['value']>0:
    data['target']=0
    data['set']=True
    data['zone']=False
    eve.set()

def cb_movelast(**kws):
  if kws['value']>0:
    data['target']=-1
    eve.set()

def cb_t1zc(**kws):
  if kws['value']>0:
    data['target']=1
    data['zone']=True
    data['set']=False
    eve.set()

def cb_t1zs(**kws):
  if kws['value']>0:
    data['target']=1
    data['zone']=True
    data['set']=True
    eve.set()

def cb_t1sc(**kws):
  if kws['value']>0:
    data['target']=1
    data['zone']=False
    data['set']=False
    eve.set()

def cb_t1ss(**kws):
  if kws['value']>0:
    data['target']=1
    data['zone']=False
    data['set']=True
    eve.set()

def cb_t2zc(**kws):
  if kws['value']>0:
    data['target']=2
    data['zone']=True
    data['set']=False
    eve.set()

def cb_t2zs(**kws):
  if kws['value']>0:
    data['target']=2
    data['zone']=True
    data['set']=True
    eve.set()

def cb_t2sc(**kws):
  if kws['value']>0:
    data['target']=2
    data['zone']=False
    data['set']=False
    eve.set()

def cb_t2ss(**kws):
  if kws['value']>0:
    data['target']=2
    data['zone']=False
    data['set']=True
    eve.set()

# epics channels
print("connecting channels...")
# commum
shotupdatepv = epics.PV("LPQ:target:shotupdate", auto_monitor=True, callback=cb_shotupdate)
enablepv = epics.PV("LPQ:target:enable", auto_monitor=True)
targetpv = epics.PV("LPQ:target:ActiveTarget", auto_monitor=True)
movepv = epics.PV("LPQ:target:movetolast", auto_monitor=True, callback=cb_movelast)
#buttons target1
t1zcpv = epics.PV("LPQ:target:1:Z1:clear", auto_monitor=True, callback=cb_t1zc)
t1zspv = epics.PV("LPQ:target:1:Z1:update", auto_monitor=True, callback=cb_t1zs)
t1scpv = epics.PV("LPQ:target:1:shot:clear", auto_monitor=True, callback=cb_t1sc)
t1sspv = epics.PV("LPQ:target:1:shot:update", auto_monitor=True, callback=cb_t1ss)
#buttons target2
t2zcpv = epics.PV("LPQ:target:2:Z1:clear", auto_monitor=True, callback=cb_t2zc)
t2zspv = epics.PV("LPQ:target:2:Z1:update", auto_monitor=True, callback=cb_t2zs)
t2scpv = epics.PV("LPQ:target:2:shot:clear", auto_monitor=True, callback=cb_t2sc)
t2sspv = epics.PV("LPQ:target:2:shot:update", auto_monitor=True, callback=cb_t2ss)
#maps
shotmap1pv = epics.PV("LPQ:target:1:shot_map", count=ASIZE, auto_monitor=False)
shotmap2pv = epics.PV("LPQ:target:2:shot_map", count=ASIZE, auto_monitor=False)
basemap1pv = epics.PV("LPQ:target:1:base_map", count=ASIZE, auto_monitor=False)
basemap2pv = epics.PV("LPQ:target:2:base_map", count=ASIZE, auto_monitor=False)
outmap1pv = epics.PV("LPQ:target:1:map", count=ASIZE, auto_monitor=False)
outmap2pv = epics.PV("LPQ:target:2:map", count=ASIZE, auto_monitor=False)
outmapcomp1pv = epics.PV("LPQ:target:1:map_comp", count=ASIZE, auto_monitor=False)
outmapcomp2pv = epics.PV("LPQ:target:2:map_comp", count=ASIZE, auto_monitor=False)

time.sleep(2)

# constants
NX = 1000
NY = 1800
mdim = (NY,NX)

# init/get the maps
print("Getting current maps")
mappvlist = [shotmap1pv, shotmap2pv, basemap1pv, basemap2pv]
maplist = []
for i in range(4):
  cmap = mappvlist[i].get()
  l=len(cmap)
  if l<ASIZE:
    print(f"Init/merge map {i+1}")
    nmap = np.zeros((ASIZE,), dtype=np.uint8)
    if l>0:
      nmap[0:l]=cmap
  else:
    print(f"map {i+1} loaded")
    nmap=cmap
  maplist.append(nmap)

for i in range(4):
  mappvlist[i].put(maplist[i])

data['enablepv']=enablepv
data['targetpv']=targetpv
data['mappvlist']=mappvlist
data['maplist']=maplist
data['mdim']=mdim
data['outmap']=[outmap1pv, outmap2pv]
data['outmapcomp']=[outmapcomp1pv, outmapcomp2pv]

## main loop
print("Target tracker service is running...")

## update compress maps
data['target']=1
compressmap(data)
data['target']=2
compressmap(data)

while True:
  eve.wait()
  try:
    tracker(data)
    compressmap(data)
  except Exception as err:
    print("[{}] Error:".format(time.asctime()))
    print(err)
    time.sleep(1)
  eve.clear()

print("OK")

